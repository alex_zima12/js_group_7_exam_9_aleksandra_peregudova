import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import axiosContacts from "../../axiosContacts";
import Spinner from "../UI/Spinner/Spinner";
import ContactItem from "../ContactItem/ContactItem";
import {fetchContacts,removeContact, setPurchasing} from "../../store/actions/contactsActions/contactsActions";
import FullContactData from "../FullContactData/FullContactData";
import Modal from "../UI/Modal/Modal";

const ContactsList = props => {

    const contacts = useSelector(state => state.contacts);
    const loading = useSelector(state => state.loading);
    const purchasing = useSelector(state => state.purchasing);
    const dispatch = useDispatch();

    const [FullContactDataObject, setFullContactData] = useState([]);

    useEffect(() => {
        dispatch(fetchContacts());
    }, [dispatch]);

    const removeContactHandler = async index => {
        dispatch(removeContact(index));
        await axiosContacts.delete('/contacts/' + index + '.json');
        dispatch(setPurchasing(false));
    };

    const editContactHandler = (id) => {
        props.history.replace( id + "/edit");
    };

    let arrFullContactData = [];
    const getFullContactData = (id) => {
        dispatch(setPurchasing(true));
        Object.keys(contacts).forEach(function (contact) {
            if (id === contact) {
             arrFullContactData.push(
                    <FullContactData
                        id={contact}
                        name={this[contact].name}
                        phone={this[contact].phone}
                        photo={this[contact].photo}
                        mail={this[contact].mail}
                        key={contact}
                        deleteItemFromContacts={() => removeContactHandler(contact)}
                        editContactHandler={() => editContactHandler(contact)}
                    />)
            }
        }, contacts);
        setFullContactData(arrFullContactData)
    };

    let arr = []
    Object.keys(contacts).forEach(function (contact) {
        arr.push(
            <ContactItem
                id={contact}
                name={this[contact].name}
                photo={this[contact].photo}
                key={contact}
                getFullContactData={() => getFullContactData(contact)}
            />)
    }, contacts);

    if (loading) {
        arr = <Spinner/>;
    }

    const purchaseCancelHandler = () => {
        dispatch(setPurchasing(false));
    };
    return (
        <div className="container">
            {arr}
            <Modal show={purchasing} closed={purchaseCancelHandler}>
                {FullContactDataObject}
            </Modal>
        </div>
    );
};

export default ContactsList;