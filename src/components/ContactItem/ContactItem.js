import React from 'react';

const ContactItem = props => {

       return (
         <div className="card w-100 d-flex flex-row p-3 mt-3"
              onClick={() => props.getFullContactData(props.id)} >
                <img src={props.photo} className="card-img-top" alt="contactPhoto" style={{width:100}}/>
                <div className="card-body  ">
                    <h5 className="card-title"> {props.name}</h5>
                </div>
        </div>

          );
};

export default ContactItem;