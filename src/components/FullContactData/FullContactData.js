import React from 'react';

const FullContactData = props => {

    return (
        <>
            <div className="card w-100 d-flex flex-row p-3 mt-3">
                <img src={props.photo} className="card-img-top" alt="contactPhoto" style={{width: 100,height:150}}/>
                <div className="card-body  ">
                    <h5 className="card-title"> Name: {props.name}</h5>
                    <p>Phone: <a className="pl-1" href={'tel:' + props.tel}>{props.phone}</a></p>
                    <p>Mail: <a className="pl-1" href={'mailto:' + props.mail} >{props.mail}</a></p>
                </div>
            </div>
            <div className="d-flex flex-row-reverse ">
                <button className="btn btn-primary m-2" type="submit"
                        onClick={() => props.editContactHandler(props.id)}
                >Edit
                </button>
                <button className="btn btn-danger m-2" type="submit"
                        onClick={() => props.deleteItemFromContacts(props.id)}
                >Delete
                </button>
            </div>
        </>
    );
};

export default FullContactData;