import {
    FETCH_CONTACTS_REQUEST,
    FETCH_CONTACTS_SUCCESS,
    FETCH_CONTACTS_FAILURE,
    SET_PURCHASING,
    REMOVE_CONTACT,
    EDIT_CONTACT

} from "../actionTypes";

import axiosContacts from "../../../axiosContacts";

const fetchContactsRequest = () => {
    return {type: FETCH_CONTACTS_REQUEST};
};
const fetchContactsSuccess = contacts => {
    return {type: FETCH_CONTACTS_SUCCESS, contacts};
};
const fetchContactsFailure = error => {
    return {type: FETCH_CONTACTS_FAILURE, error};
};

export const fetchContacts = () => {
    return async dispatch => {
        dispatch(fetchContactsRequest());
        try {
            const response = await axiosContacts.get('/contacts.json');
            dispatch(fetchContactsSuccess(response.data));
        } catch(e) {
            dispatch(fetchContactsFailure(e));
        }
    };
};

export const setPurchasing = purchasing => {
    return {type: SET_PURCHASING, purchasing: purchasing}
};

export const removeContact = (index) => {
    return {type: REMOVE_CONTACT, index};
};

export const editContact = (id, contact) => {
    return {type: EDIT_CONTACT, id, contact};
};
