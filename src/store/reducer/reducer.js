import {
    FETCH_CONTACTS_REQUEST,
    FETCH_CONTACTS_SUCCESS,
    FETCH_CONTACTS_FAILURE,
    SET_PURCHASING,
    REMOVE_CONTACT,
    EDIT_CONTACT
} from "../actions/actionTypes";

const initialState = {
    loading: false,
    error: null,
    fetchError: null,
    contacts: [],
    purchasing: false
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_CONTACTS_REQUEST:
            return {
                ...state,
                loading: true
            };
        case FETCH_CONTACTS_SUCCESS:
            return {
                ...state,
                loading: false,
                contacts: action.contacts
            };
        case FETCH_CONTACTS_FAILURE:
            return {
                ...state,
                loading: false,
                fetchError: action.error
            };
        case SET_PURCHASING:
            return {
                ...state,
                purchasing: action.purchasing
            };
        case REMOVE_CONTACT:
            let copyStateContacts = {...state.contacts};
            delete copyStateContacts[action.index]
            return {
                ...state,
                contacts: {...copyStateContacts}
            }
        case EDIT_CONTACT:
            let copyContacts = {...state.contacts};
            Object.keys(copyContacts).forEach(function (contact) {
                    if (action.id === contact) {
                        return {
                            ...state,
                            contacts: action.contact
                        }
                    }
                }
            ); return state
        default:
            return state;
    }
};

export default reducer;