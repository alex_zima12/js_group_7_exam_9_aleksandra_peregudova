import axios from 'axios';

const axiosContacts = axios.create({
    baseURL: 'https://jsgroup7exam.firebaseio.com/'
});

export default axiosContacts;