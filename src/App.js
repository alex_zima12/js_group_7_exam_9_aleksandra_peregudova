import React from 'react';
import {Route, Switch} from 'react-router-dom';
import Layout from "./components/Layout/Layout";
import AdditionNewContact from "./containers/AdditionNewContact/AdditionNewContact";
import ContactsList from "./components/ContactsList/ContactsList";
import EditContact from "./containers/EditContact/EditContact";


const App = () => (
    <Layout>
      <Switch>
        <Route path="/" exact component={ContactsList}/>
        <Route path="/addition" component={AdditionNewContact}/>
          <Route path="/:id/edit" component={EditContact}/>
        <Route render={() => <h1>404 Not Found</h1>}/>
      </Switch>
    </Layout>
);

export default App;
