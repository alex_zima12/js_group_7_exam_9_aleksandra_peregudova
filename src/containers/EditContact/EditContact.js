import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import axiosContacts from "../../axiosContacts";
import Spinner from "../../components/UI/Spinner/Spinner";
import {editContact, setPurchasing} from "../../store/actions/contactsActions/contactsActions";

const EditContact = props => {

       const loading = useSelector(state => state.loading);
    const contacts = useSelector(state => state.contacts);
    const dispatch = useDispatch();

    const [newName, setNewName] = useState('');
    const [newPhone, setNewPhone] = useState('');
    const [newPhoto, setNewPhoto] = useState('');
    const [newMail, setNewMail] = useState('');

    useEffect(() => {
        Object.keys(contacts).forEach(function (contact) {
            if (props.match.params.id  === contact) {
                setNewName(contacts[contact].name);
                setNewPhone(contacts[contact].phone);
                setNewPhoto(contacts[contact].photo);
                setNewMail(contacts[contact].mail)
            }
        })
    },[dispatch,contacts,props.match.params.id ]);

    const changeName = event => {
        setNewName(event.target.value);
    };

    const changePhone = event => {
        setNewPhone(event.target.value);
    };

    const changePhoto = event => {
        setNewPhoto(event.target.value);
    };

    const changeMail = event => {
        setNewMail(event.target.value);
    };

    const contact = {
        name: newName,
        phone: newPhone,
        mail: newMail,
        photo: newPhoto
    };

    const changeContact = async (e, id)  => {
        e.preventDefault()
        await axiosContacts.put('/contacts/' + id + '.json', contact);
        dispatch(editContact(id,contact));
        props.history.push('/');
    };

    const goBackToContacts = async event => {
        event.preventDefault();
        dispatch(setPurchasing(false));
        props.history.replace('/');
    };

    let arrPhoto = []
    if (newPhoto){
        arrPhoto = <img src={newPhoto} alt="Person"/> }

    let form = (
        <form className="container">
            <div className="form-group">
                <label>Name</label>
                <input
                    className="form-control" placeholder="Enter name"
                    type="text" name="name"
                    value={newName}
                    onChange={changeName}
                    required
                />
            </div>
            <div className="form-group">
                <label>Phone</label>
                <input
                    className="form-control" placeholder="Enter phone number"
                    type="tel" name="phone"
                    value={newPhone}
                    onChange={changePhone}
                    required
                />
            </div>
            <div className="form-group">
                <label>Mail</label>
                <input
                    className="form-control" placeholder="Enter mail"
                    type="email" name="mail"
                    value={newMail}
                    onChange={changeMail}
                    required
                />
            </div>
            <div className="form-group">
                <label>Photo</label>
                <input
                    className="form-control" placeholder="https://example.com"
                    type="url" name="photo"
                    value={newPhoto}
                    onChange={changePhoto}
                    required
                />
            </div>
            <div className="m-3">
                {arrPhoto}
            </div>

            <button
                className="btn btn-primary"
                type="submit"
                onClick={(event) => changeContact(event, props.match.params.id)}
            >Save</button>
            <button
                className="btn btn-danger ml-3"
                onClick={goBackToContacts}
            >CANCEL
            </button>
        </form>
    );

    if (loading) {
        form = <Spinner/>;
    }

    return (
        <div className="container mt-3">
            <h4>New contact</h4>
            {form}
        </div>
    );
}



export default EditContact;