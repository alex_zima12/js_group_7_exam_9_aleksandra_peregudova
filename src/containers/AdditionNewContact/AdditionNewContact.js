import React, {useState} from 'react';
import {useSelector} from "react-redux";
import axiosContacts from "../../axiosContacts";
import Spinner from "../../components/UI/Spinner/Spinner";

const AdditionNewContact = props => {

    const loading = useSelector(state => state.loading);
    const [newContact, setNewContact] = useState({
        name: '',
        phone: '',
        mail: '',
        photo: ''

    });

    const newContactChanged = event => {
        const name = event.target.name;
        const value = event.target.value;
        setNewContact(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    const newContactHandler = async event => {
        event.preventDefault();
        await axiosContacts.post('/contacts.json', newContact);
        alert("Data recorded in contacts")
    };

    const goBackToContacts = async event => {
        event.preventDefault();
        props.history.replace('/');
    };

    let arrPhoto = []
    if (newContact.photo){
        arrPhoto = <img src={newContact.photo} alt="Person"/> }

    let form = (
        <form className="container">
            <div className="form-group">
                <label>Name</label>
                <input
                    className="form-control" placeholder="Enter name"
                    type="text" name="name"
                    value={newContact.name}
                    onChange={newContactChanged}
                    required
                />
            </div>
            <div className="form-group">
                <label>Phone</label>
                <input
                    className="form-control" placeholder="Enter phone number"
                    type="tel" name="phone"
                    value={newContact.phone}
                    onChange={newContactChanged}
                    required
                />
            </div>
            <div className="form-group">
                <label>Mail</label>
                <input
                    className="form-control" placeholder="Enter mail"
                    type="email" name="mail"
                    value={newContact.mail}
                    onChange={newContactChanged}
                    required
                />
            </div>
            <div className="form-group">
                <label>Photo</label>
                <input
                    className="form-control" placeholder="https://example.com"
                    type="url" name="photo"
                    value={newContact.photo}
                    onChange={newContactChanged}
                    required
                />
            </div>
            <div className="m-3">
            {arrPhoto}
            </div>
            <button
                className="btn btn-primary"
                onClick={newContactHandler}
            >SAVE
            </button>
            <button
                className="btn btn-danger ml-3"
                onClick={goBackToContacts}
            >CANCEL
            </button>
        </form>
    );

    if (loading) {
        form = <Spinner/>;
    }

    return (
        <div className="container mt-3">
            <h4>New contact</h4>
            {form}
        </div>
    );
}



export default AdditionNewContact;